//
//  AppDelegate.h
//  MyLib
//
//  Created by Naz Mariano on 08/07/2016.
//  Copyright © 2016 Naz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


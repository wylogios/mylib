//
//  main.m
//  MyLib
//
//  Created by Naz Mariano on 08/07/2016.
//  Copyright © 2016 Naz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
